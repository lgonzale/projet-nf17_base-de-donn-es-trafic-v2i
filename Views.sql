--Goal: Lister tous les véhicules dans une région

CREATE VIEW Cars_for_Region AS
    SELECT Veh.num_immatriculation, Veh.modele->>'marque' AS Marque
    FROM Vehicule AS Veh
    INNER JOIN interchange_message AS Mes ON Veh.num_immatriculation= Mes.num_immatriculation
    INNER JOIN Commune as Com ON Mes.id_communee = Com.id_communee
    WHERE Com.nom_communee = 'Paris';
    
--Goal: Type de weather au cours du temps par station    
CREATE VIEW Weather_for_Station AS
    SELECT Alert_M.dates, Alert_M.weather, Alert_M.temperature
    FROM Alertes_Meteo AS Alert_M
    INNER JOIN Capteur AS Cap ON Cap.numero = Alert_M.capteur 
    WHERE Cap.id_station = 1;

--Goal: Accidents detectées par capteur   
CREATE VIEW Accidents_for_Capteur AS
    SELECT Acc.id_accident, Rel_Acc.num_immatriculation, Acc.gravite
    FROM Accidents AS Acc
    INNER JOIN relAccidents AS Rel_Acc ON Acc.id_accident = Rel_Acc.id_accident 
    WHERE capteur = 1;

--Goal: Toutes les communications liées à une station de base

CREATE VIEW Messages_for_Station AS 
    SELECT Mes.num_immatriculation, Mes.type_contenu, Mes.contenu, Mes.dates, Mes.locationn
    FROM interchange_message AS Mes
    INNER JOIN Stations_de_Base AS Stat ON Mes.id_station = Stat.id_station
    WHERE Mes.id_station= 1;
    
--Goal: Toutes les communications liées à une voiture

CREATE VIEW Messages_for_Car AS
    SELECT Mes.type_contenu, Mes.contenu, Mes.dates, Mes.locationn
    FROM interchange_message AS Mes 
    WHERE Mes.num_immatriculation = 'ZZ-BB-97';
    
--Goal: Nombre d'accidents par gravite
CREATE VIEW Stats_Accidents_par_Gravite AS
    SELECT COUNT(ACC.id_accident), ACC.gravite
    FROM Accidents as ACC
    GROUP BY ACC.gravite;

--Goal: Temperature moyenne par climat (cloudy, sunny, etc)
CREATE VIEW Stats_Temperature_par_Weather AS
    SELECT AVG(Ale_M.Temperature), Ale_M.weather
    FROM Alertes_Meteo as Ale_M
    GROUP BY Ale_M.weather;
    
--Goal: Distribution des vehicules par années.

CREATE VIEW Vehicules_Antiquity_Distribution AS
    SELECT CAST (Veh.modele->>'annee' AS INTEGER) AS Année, COUNT( CAST (Veh.modele->>'annee' AS INTEGER)) AS Total
    FROM Vehicule as Veh
    GROUP BY CAST (Veh.modele->>'annee' AS INTEGER)
    ORDER BY CAST (Veh.modele->>'annee' AS INTEGER) DESC;
    
    
--DELETE: supprimer de la base toutes les communications qui concernent une station de base particulière. Dans ce cas le id_station 1
DELETE FROM Interchange_Message WHERE id_station = 1;

    