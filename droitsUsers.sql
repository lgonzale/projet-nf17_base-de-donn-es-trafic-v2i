-- Attribution des droits

CREATE USER adminis;
CREATE USER personne_decision;

-- Le adminis seront des administrateur système, ils pourront modifier, insérer et supprimer des valeurs dans la base de données et la consulter.
GRANT ALL ON TABLE  
Vehicule, Commune, Stations_de_Base, Accidents, Alertes_Meteo, Capteur , relaccidents , interchange_message  TO adminis;

-- Un personne_decision pourra consulter la base de données.

GRANT SELECT ON TABLE  
Vehicule, Commune, Stations_de_Base, Accidents, Alertes_Meteo, Capteur , relaccidents , interchange_message TO personne_decision;
