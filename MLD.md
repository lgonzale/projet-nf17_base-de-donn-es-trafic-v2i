# Préliminaires

L'idée est de normaliser la base de données en 3NF avec l'objectif de réduire la redundance. Seulement pour réviser les trois formes normalles:     

- **Décomposition 0NF->1NF:** Soit R une relation. Si R contient un attribut non atomique portant sur des valeurs hétérogènes (correspondant à un attribut composé au niveau conceptuel), alors l'attribut est décomposé en plusieurs attributs a1...an.     

- **Décomposition 1NF->2NF:** Soit R une relation comportant une clé composée de k1 et k1'. Si R contient une DFE de k1' vers des attributs n'appartenant pas à la clé, alors R est décomposée en R1 et R2, tel que :
    - R1 est R moins les attributs déterminés par k1' et avec k1' clé étrangère vers R2
    - R2 est composée de k1' et des attributs qu'elle détermine, avec k1' clé primaire de R2

- **Décomposition 2NF->3NF:** Soit R une relation comportant une DFE de a vers b qui n'appartiennent pas à une clé, alors R est décomposée en R1 et R2, tel que :
    - R1 est R moins les attributs déterminés par a et avec a clé étrangère vers R2
    - R2 est composée de a et des attributs qu'elle détermine, avec a clé primaire de R2

# Dans le cadre du projet

## Liste de Relations

On a 8 tables (relations)

On va utilise un type ENUMERE: Voitures_speciale qui contient les valeurs {Pompier, Police, samu, autre}

1. Vehicule(#numImmatriculation:varchar, {annee : integer, marque : varchar, modele : varchar, capMotor : integer}: Json, capMax: integer, type : voitureSpeciale, voitureTransporteePar: String) avec annee, marque et modele obligatoire.

Quelques constraints particulières:

    - Voiture: {voiture_transportee_par, type, capMotor, capMax} *interdit*

    - Moto: {capMotor} *obligatoire* && {capMax, type, voiture_transportee_par}  *interdit*

    - Camion: {capMax} *obligatoire* && {type, capMotor, voiture_transportee_par} *interdit*

    - Vehicules speciales: {type} *obligatoire* && {capMotor, capMax, voiture_transportee_par}  *interdit* 
    
    - voitures trouvées dans des camions: (voiture_transportee_par} *obligatoire* && {type, capMotor, capMax} *interdit*



2. Accidents(#id_accident : integer, dates : date, capteur->Capteur, idCommunee=> Communne, gravite : gravite, idVehicule) avec tous les champs obligatoires.        

3. AlertesMeteo (# id_alerte : integer, dates : date, capteur->Capteur, weather :varchar, temperature: integer, idCommunee=> Commune) avec tous les champs obligatoires 

4. Commune(#idCommunee : integer, nom : varchar, cp: Integer) avec tous les champs obligatoires.

5. StationDeBase(#id_station : integer, long : varchar, lat : varchar, idCommunee=>commune) avec tous les champs obligatoires.

6. Capteur(#numero : integer, modele : varchar, id_station => StationDeBase, numImmatriculation => Vehicule) avec 1 SEUL condition {idStation, numImmatriculation} obligatoire.

7. interchangeMessage(#numImmatriculation => Vehicule, #idStation => StationDeBase, contenu : varchar, type_contenu : varchar, dates : Date, locationn: Json ) avec tous les champs obligatoires.

8. relAccidents(#numImmatriculation=>Voiture, #idAccident=>Accidents) 

## Heritage

La **Relation 1**: Héritage par la classe mère pour Vehicule 

Les **Relations 2 et 3**: Héritage par la classe fille pour Accidents, AlertesMeteo 

## Normalisation

### Règle génerale

1FN est inclus dans 2FN && 2FN est inclus dans 3FN.

1FN: Respecte les règles a et b

2FN: Respecte les règles a et b et c

3FN: Respecte les règles a et b et c et d

### Codage

On va définir le codage suivant :

Si on met 1: Vérifiée

Si on met 0: Pas validée

### Conditions

a. Au moins une clé

b  Tous ses attributs sont atomiques

c. Tout attribut n'appartenant à aucune clé candidate ne dépend pas d'une partie seulement d'une clé candidate

d. Tout attribut n'appartenant à aucune clé candidate ne dépend directement que de clés candidates

### Evaluation

- R1: Vehicule {(a,1), (b,1),(c,1),(d,0)} -> 2FN
- R2: Accidents {(a,1), (b,1),(c,1),(d,0)} -> 2FN
- R3: AlertesMeteo {(a,1), (b,1),(c,1),(d,0)} -> 2FN
- R4: Commune {(a,1), (b,1),(c,1),(d,1)} -> 3FN
- R5: StationDeBase {(a,1), (b,1),(c,1),(d,0)} -> 2FN 
- R6: Capteur {(a,1), (b,1),(c,1),(d,1)} -> 3FN
- R7: interchangeMessage {(a,1), (b,1),(c,1),(d,0)} -> 2FN
- R8: relAccidents {(a,1), (b,1),(c,0),(d,0)} -> 1FN
 

## Constraintes au niveau logique-relationelles

- PROJECTION(interchangeMessage, numImmatriculation) = PROJECTION(Vehicule, numImmatriculation)
- PROJECTION(interchangeMessage, idStation) = PROJECTION(StationDeBase, idStation)
- PROJECTION(Capteur, numero) = PROJECTION(AlertesMeteo, capteur) 
- PROJECTION(Capteur, numero) = PROJECTION(Accidents, capteur)
- PROJECTION(Accidents, id_accident) = PROJECTION(relAccidents, idAccident)
- PROJECTION(Accidents, numImmatriculation) = PROJECTION(Vehicule, numImmatriculation)
- Accidents.id_accident NOT NULL
- AlertesMeteo.id_alerte NOT NULL 
- PROJECTION(Accidents, idCommunee) = PROJECTION(Commune, idCommunee)
- PROJECTION(AlertesMeteo, idCommunee) = PROJECTION(Commune, idCommunee)

## Views

- **Cars_for_Region :** On a besoin de trois rélations: {Vehicule, interchange_message, Commune} avec Inner Join Vehecule.numImatriculation= interchangeMessage.numImmatriculation et interchangeMessage.idCommunee = Commune.idCommunee 
- **Weather_for_Station :** On a besoin de deux rélations: {Alertes_Meteo, Capteur} avec Inner Join Capteur.numero = Alert_Meteo.capteur 
- **Accidents_for_Capteur :** On a besoin de deux rélations: {Accidents, relAccidents} avec Inner Join  Accidents.id_accident = relAccidents.id_accident 
- **Messages_for_Station :** On a besoin de deux rélations: {interchange_message, Stations_de_Base } avec Inner Join interchange_message.id_station = Stations_de_Base.id_station
- **Messages_for_Car :** On a besoin d'un rélation: {interchange_message} avec Where pour donner un numero de immatriculation spécifique.
- **Stats_Accidents_par_Gravite:** On a besoin d'un rélation: {Accidents} avec l'aggregat COUNT et GROUP BY pour savoir le nombre d'accidents total par gravité  
- **Stats_Temperature_Par_Weather:** On a besoin d'un rélation: {Alertes_Meteo} avec l'aggregat AVG et GROUP BY pour savoir la temperature moyenne d'accord au type de climat.
- **Vehicules_Antiquity_Distribution** On a besoin d'un rélation: {Vehicule} avec l'aggregat COUNT et GROUP BY sur l'attribute Json:{modele -> année}


## Intégration de Json dans la BDD

- **Relation 1:** Modele {annee : integer, marque : varchar, modele : varchar}: Json

- **Relation 7:** locationn {long: integer, lat:integer}: Json