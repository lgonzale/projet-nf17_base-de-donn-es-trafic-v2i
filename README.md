# Base de données: Trafic V2I

## Applications des connaissances de NF17 

Dans ce projet, l'objectif est d'appliquer la bonne méthodologie pour créer une base de données rélationelles vues dans le cadre de l'UV NF17. Les étapes à réaliser comprennent:

- **Note de Clarification (NDC)** pour analyser bien situation existante (problème à résoudre) et des besoins particulières du client.

- **Modèle Conceptuel (MCD)** qui permet de représenter tous les aspects de manière simplifiée du problème. 

- **Modèle Logique (MLD)** qui décrire la solution, en prenant une orientation informatique générale (type de SGBD dans ce cas), formelle, mais indépendamment de choix d'implémentation.

- **Implémentation d'une base de données dans SQL**: Create, Insert and Select. Aussi PostgreSQL/JSON. 

## Problème a Résoudre: Trafic V2I

Appliquer les notions de bases de données pour modéliser une solution capable de stocker et d'accéder aux informations qui circulent dans une infrastructure V2I dans un réseau de véhicules de dernière génération. 

## Organisation du projet

- **Cahier des charges:** Cette section contient les besoins et les hypothèses du client sur le problème à résoudre à l'aide d'une base de données (basé sur le sujet 31: https://librecours.net/dawa/projets-2020-II/co/v2i.html)
- **Note de Clarification:** Liste des objets, propriétés, contraintes, utilisateurs et fonctions.
- **MCD:** Diagramme UML. 
- **MLD:** Modèle logique basé sur le MCD.
- **Create_BD.sql:** Création des relations
- **insert_data.sql** Ajouter des valeurs aux rélations
- **Views.sql** Définition des views
- **droitsUser.sql** Définition des droits pour les  2 types de utilisateurs: Admin, Personne_decision

## Information Génerale

**Client:** Alessandro Victorino

**Étudiant:** Luis Enrique González Hilario

**Duration du Projet:** 3 semaines: 27 mai - 17 juin 