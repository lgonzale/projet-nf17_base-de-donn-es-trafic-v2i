CREATE TYPE Voitures_speciale as ENUM ('Pompier', 'Police', 'samu', 'autre');

CREATE TABLE Vehicule(
    num_immatriculation VARCHAR NOT NULL PRIMARY KEY,
    modele JSON NOT NULL,
    cap_motor INTEGER,
    cap_max INTEGER,
    type Voitures_speciale,
    voiture_transportee_par VARCHAR,
    FOREIGN KEY (voiture_transportee_par) REFERENCES Vehicule(num_immatriculation),
    CHECK (
    (voiture_transportee_par IS NULL AND cap_max IS NULL AND cap_motor IS NULL AND type IS NULL) OR
    (voiture_transportee_par IS NULL AND cap_max IS NULL AND cap_motor IS NOT NULL AND type IS NULL) OR
    (voiture_transportee_par IS NULL AND cap_max IS NOT NULL AND cap_motor IS NULL AND type IS NULL) OR
    (voiture_transportee_par IS NULL AND cap_max IS NULL AND cap_motor IS NULL AND type IS NOT NULL) OR 
    (voiture_transportee_par IS NOT NULL AND cap_max IS NULL AND cap_motor IS NULL AND type IS NULL))
);


CREATE TABLE Commune(
    id_communee INTEGER PRIMARY KEY,
    nom_communee VARCHAR NOT NULL,
    cp INTEGER NOT NULL
);

CREATE TABLE Stations_de_Base (
    id_station INTEGER PRIMARY KEY,
    long VARCHAR NOT NULL,
    lat VARCHAR NOT NULL,
    id_communee INTEGER NOT NULL,
    FOREIGN KEY (id_communee) REFERENCES Commune(id_communee)
);

CREATE TABLE Accidents (
    id_accident INTEGER PRIMARY KEY,
    dates DATE NOT NULL,
    capteur INTEGER NOT NULL,
    id_communee INTEGER NOT NULL, 
    gravite VARCHAR NOT NULL,
    FOREIGN KEY (id_communee) REFERENCES Commune(id_communee)

);


CREATE TABLE Alertes_Meteo (
    id_alerte INTEGER PRIMARY KEY,
    dates DATE NOT NULL,
    capteur INTEGER NOT NULL,
    weather VARCHAR NOT NULL,
    temperature INTEGER NOT NULL,
    id_communee INTEGER NOT NULL, 
    FOREIGN KEY (id_communee) REFERENCES Commune(id_communee)
);

CREATE TABLE Capteur (
    numero INTEGER PRIMARY KEY,
    modele VARCHAR NOT NULL,
    id_station INTEGER,
    num_immatriculation VARCHAR,
    FOREIGN KEY (id_station) REFERENCES Stations_de_Base(id_station),
    FOREIGN KEY (num_immatriculation) REFERENCES Vehicule(num_immatriculation),
    CHECK ((num_immatriculation IS NULL AND id_station is NOT NULL) OR (num_immatriculation IS NOT NULL AND id_station is NULL))
);

CREATE TABLE relAccidents (
    num_immatriculation VARCHAR,
    id_accident INTEGER,
    PRIMARY KEY (num_immatriculation, id_accident),
    FOREIGN KEY (num_immatriculation) REFERENCES Vehicule(num_immatriculation),
    FOREIGN KEY (id_accident) REFERENCES Accidents(id_accident)
);


CREATE TABLE interchange_message (
    num_immatriculation VARCHAR,
    id_station INTEGER,
    type_contenu VARCHAR NOT NULL,
    contenu VARCHAR NOT NULL,
    dates DATE NOT NULL,
    locationn JSON NOT NULL,
    id_communee INTEGER NOT NULL,
    PRIMARY KEY (num_immatriculation, id_station),
    FOREIGN KEY (num_immatriculation) REFERENCES Vehicule(num_immatriculation),
    FOREIGN KEY (id_station) REFERENCES Stations_de_Base(id_station)
);
