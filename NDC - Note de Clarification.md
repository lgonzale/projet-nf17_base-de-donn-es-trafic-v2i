# Note de Clarification

Ce document a pour objetif préciser les éléments du projet, qui résulte de la reformulation du cahier des charges. Cela servira de référence pour les autres phases ultérieures.


## Objets gérés dans la BD

- **Vehicules:** Il répresente 1 véhicule
- **Voitures:** Type de Véhicule
- **Motos:** Type de Véhicule
- **Camions:** Type de Véhicule
- **Voitures spéciales:** Type de Véhicule
- **Pompier:** Exemple de Voitures spéciales
- **Police:** Exemple de Voitures spéciales
- **SAMU:** Exemple de Voitures spéciales
- **Autres:** Exemple de Voitures spéciales
- **Stations de base:** Il répresente 1 station de base
- **Capteur:** Il répresente a capteur
- **Evenement:** Il répresent a evenement
- **Accidents:** Exemple d'événement
- **Alertes météo:** Exemple d'événement
- **Commune:** Il represente a commune
- **interchange_message:** Il répresente la communication entre entités

## Attributes/Propriétés associées aux objets

Vehicule:

        num_immatriculation (int, obligatoire)
        
        marque (string)
        
        modele (string)
        
        annee_production (int)
        
    

    Voiture: 
    
        Il peut avoir la propriété __Voiture_transporte_par__ seulement s'il s'agit d'un voiture transporte par un camion.
    
    Motos:
    
        cap_motor (int)
    
    Camion:

        cap_max (int)
    
    Voitures spéciale:
    
        type({Pompier, Police, samu, autre})
    

Stations de base:

        id (int, obligatoire)
        
        lat (int)
        
        long (int)
        
        
Capteur:

        modele (string, obligatoire)
        
        numero (int, obligatoire)
        

Commune: 

        id_commune (int)
        
        nom_commune (string)
        
        code_postal (int)
        

Evenement:

        id(obligatoire, int)
        
        dates (timestamp)
        
        type ({Accidents, Alertes météo})
        

Alertes météo: 

        id_alerte (obligatoire, int)
        
        weather (string)
        
        temperature (int)
        
        
        
Accidents: 

        id_accident (int, obligatoire)
        
        gravité (string)
        

interchange_message:

        date (TimeStamp)
        contenu (String)
        type_contenu (String)


## Contraintes associées aux objets et propriétés

### Hèritage 

- Les objets {Voiture, Motos, Camion, Voitures spéciale} herèdent les propriètes de Vehicule
- Les objets {Alertes météo,Accidents} herèdent les propriètes de Evenement

### Associations

- Evenement **est associée** à Commune
- Evenement **est associée** à Capteur
- Commune **est associée** à Stations de base
- Stations de Base **est associée** à Vehicule
- intechange_message **est associée** à deux tables: stations de base, vehicules. 

### Cardinalité

- Les véhicules peuvent envoyer des informations vers la station (0..n, 0..n)
- Les stations de base peuvent communiquent entre elles et envoyer des informations aux véhicules (1 à 1..n)
- Les stations de base contiennent forcément au moins un capteur.
- 1 camion peut o pas transporter voitures 
- 1 commune peut avoir plusieurs stations de base
- Les véhicules son equipes de capteurs (1..n, 1..n)
- Les stations de base possedent capteurs (0..1, 1..n)

## Utilisateurs appelés à modifier et consulter les données

- **administrateur:** Cet utilisateur peut contrôler toute la base de données.
- **personne_decision:** Un utilisateur qui aide à les decision peut consulter les sections de la base de données mais pas supprimer.

## Fonctions que ces utilisateurs pourront effectuer

L'administrateur et la personne_decision peut obtenir un list de tous les véhicules dans une région

L'administrateur et la personne_decision peut obtenir un list de toutes les communications liées à une véhicule ou à une station de base

L'administrateuret la personne_decision peut trouver le véhicule le plus proche d'un certain type, par exemple, un camion de pompiers, véhicule SAMU, etc. ou un certain modèle : toutes les Citroën C5, ...

L'administrateur et la personne_decision peut calculer les statistiques de passage de véhicules : par zone, par type, ...

L'administrateur peut supprimer de la base toutes les communications qui concernent un certain modèle du capteur (par exemple, dont le mal-fonctionnement a été démontré).

L'administrateur peut supprimer de la base toutes les communications qui concernent une station de base particulière (modèle défaillant).

## Vues

- **Cars_for_Region :** Lister tous les véhicules dans une région spécifique
- **Weather_for_Station :** Toutes les alertes méteo dans 1 station de base spécifique
- **Accidents_for_Capteur :** Toutes les accidents détectes par 1 capteur spécifique
- **Messages_for_Station :** Toutes les communications liées à une station de base spécifique
- **Messages_for_Car :** Toutes les communications liées à une voiture spécifique
- **Stats_Accidents_par_Gravite:** Il count le nombre d'accidents agroupées par gravité
- **Stats_Temperature_Par_Weather:** Temperature moyenne par type de climat (cloudy, sunny, etc)
- **Vehicules_Antiquity_Distribution** Distribution des années des differents vehicules inscrits

## Intégration de Json dans la BDD

- **Relation 1:** Modele {annee : integer, marque : varchar, modele : varchar}: Json

- **Relation 7:** locationn {long: integer, lat:integer}: Json
