--Motos
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_motor") VALUES ('ZZ-00-WW', '{"annee" : 2010, "marque":"italica", "modele" : "modele 10" }', 80);
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_motor") VALUES ('ZZ-00-PP', '{"annee" : 2011, "marque":"italica", "modele" : "modele 4" }', 70);
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_motor") VALUES ('ZZ-00-ST', '{"annee" : 2013, "marque":"italica", "modele" : "modele 21" }', 90);
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_motor") VALUES ('ZZ-00-SP', '{"annee" : 2017, "marque":"italica", "modele" : "modele 22" }', 60);
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_motor") VALUES ('ZZ-01-RP', '{"annee" : 2015, "marque":"Kawasaki", "modele" : "modele pro" }', 100);
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_motor") VALUES ('ZZ-02-RP', '{"annee" : 2016, "marque":"Kawasaki", "modele" : "modele x" }', 90);

--Camions
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_max") VALUES ('ZZ-00-12','{"annee" : 2014, "marque":"Volvo", "modele" : "modele 5" }', 12);
INSERT INTO Vehicule("num_immatriculation", "modele", "cap_max") VALUES ('ZZ-YX-12','{"annee" : 2018, "marque":"Volvo", "modele" : "modele 10" }', 10);


--Voitures spéciales
INSERT INTO Vehicule("num_immatriculation", "modele", "type") VALUES ('ZZ-00-13', '{"annee" : 2019, "marque":"volt", "modele" : "modele 8" }', 'Police');
INSERT INTO Vehicule("num_immatriculation", "modele", "type") VALUES ('ZZ-00-33', '{"annee" : 2020, "marque":"volt", "modele" : "modele z" }', 'Pompier');
INSERT INTO Vehicule("num_immatriculation", "modele", "type") VALUES ('ZZ-00-38', '{"annee" : 2015, "marque":"volt", "modele" : "modele s" }', 'samu');
INSERT INTO Vehicule("num_immatriculation", "modele", "type") VALUES ('ZZ-00-43', '{"annee" : 2019, "marque":"volt", "modele" : "modele cc" }', 'autre');


--Voitures normales
INSERT INTO Vehicule("num_immatriculation", "modele") VALUES ('ZZ-00-97', '{"annee" : 2020, "marque":"Ferrari", "modele" : "modele y" }');
INSERT INTO Vehicule("num_immatriculation", "modele") VALUES ('ZZ-BB-97', '{"annee" : 2020, "marque":"Bugatti", "modele" : "modele b" }');
INSERT INTO Vehicule("num_immatriculation", "modele") VALUES ('ZZ-BB-82', '{"annee" : 2020, "marque":"Fiat", "modele" : "modele f" }');
INSERT INTO Vehicule("num_immatriculation", "modele") VALUES ('ZZ-BW-04', '{"annee" : 2018, "marque":"Nissan", "modele" : "modele v" }');
INSERT INTO Vehicule("num_immatriculation", "modele") VALUES ('ZZ-BX-56', '{"annee" : 2017, "marque":"Nissan", "modele" : "modele 12" }');
INSERT INTO Vehicule("num_immatriculation", "modele") VALUES ('ZZ-AB-73', '{"annee" : 2015, "marque":"Nissan", "modele" : "modele 0" }');


--Voitures transportes par camions
INSERT INTO Vehicule("num_immatriculation", "modele", "voiture_transportee_par") VALUES ('ZZ-22-95', '{"annee" : 2020, "marque":"Bugatti", "modele" : "modele yy" }', 'ZZ-00-12');
INSERT INTO Vehicule("num_immatriculation", "modele", "voiture_transportee_par") VALUES ('ZZ-24-97', '{"annee" : 2020, "marque":"Fiat", "modele" : "modele fy" }', 'ZZ-00-12');
INSERT INTO Vehicule("num_immatriculation", "modele", "voiture_transportee_par") VALUES ('ZR-24-90', '{"annee" : 2019, "marque":"Lamborghini", "modele" : "modele py" }', 'ZZ-YX-12');
INSERT INTO Vehicule("num_immatriculation", "modele", "voiture_transportee_par") VALUES ('RZ-24-91', '{"annee" : 2019, "marque":"Lamborghini", "modele" : "modele r7" }', 'ZZ-YX-12');


--Comunes
INSERT INTO Commune VALUES (1,'Paris', 75003);
INSERT INTO Commune VALUES (2,'Montepellier', 34000);
INSERT INTO Commune VALUES (3,'Aisne', 02100);
INSERT INTO Commune VALUES (4,'La Rochelle', 17000);


--Capteurs cars
INSERT INTO Capteur("numero", "modele", "num_immatriculation") VALUES (1, 'ZZZZTT','ZZ-00-97');
INSERT INTO Capteur("numero", "modele", "num_immatriculation") VALUES (3, 'pppTT','ZZ-BB-97');
INSERT INTO Capteur("numero", "modele", "num_immatriculation") VALUES (5, 'Wt0233','ZZ-00-97');
INSERT INTO Capteur("numero", "modele", "num_immatriculation") VALUES (7, 'rq1221','ZZ-BB-97');


--Stations de base
INSERT INTO Stations_de_Base VALUES (1, '2.3488', '48.8532', 1);
INSERT INTO Stations_de_Base VALUES (2, '4.3488', '49.8531', 1);
INSERT INTO Stations_de_Base VALUES (3, '3.3488', '50.8534', 2);


--Capteurs id_station
INSERT INTO Capteur("numero", "modele", "id_station") VALUES (2, 'UUUUVVV',1);
INSERT INTO Capteur("numero", "modele", "id_station") VALUES (4, 'UUUUVV2',2);


--Accidents
INSERT INTO Accidents VALUES (1, '2020-05-30', 1, 1, 'low');
INSERT INTO Accidents VALUES (2, '2020-06-01', 1, 1, 'high');
INSERT INTO Accidents VALUES (3, '2020-06-02', 1, 1, 'high');
INSERT INTO Accidents VALUES (4, '2020-06-03', 1, 1, 'medium');
INSERT INTO Accidents VALUES (5, '2020-06-06', 1, 1, 'low');
INSERT INTO Accidents VALUES (6, '2020-06-07', 1, 1, 'low');
INSERT INTO Accidents VALUES (7, '2020-06-09', 1, 1, 'medium');

--Alerts méteo
INSERT INTO Alertes_Meteo VALUES (1, '2020-05-29', 1, 'cloudy', 14, 2);
INSERT INTO Alertes_Meteo VALUES (2, '2020-06-01', 1, 'sunny', 28, 1);
INSERT INTO Alertes_Meteo VALUES (3, '2020-06-02', 1, 'foogy', 09, 1);
INSERT INTO Alertes_Meteo VALUES (4, '2020-06-04', 2, 'raining', 13, 2);
INSERT INTO Alertes_Meteo VALUES (5, '2020-06-05', 2, 'cloudy', 08, 1);
INSERT INTO Alertes_Meteo VALUES (6, '2020-06-07', 2, 'cloudy', 11, 1);
INSERT INTO Alertes_Meteo VALUES (7, '2020-06-03', 1, 'raining', 11, 2);
INSERT INTO Alertes_Meteo VALUES (8, '2020-06-04', 1, 'sunny', 29, 1);
INSERT INTO Alertes_Meteo VALUES (9, '2020-06-05', 1, 'foogy', 06, 1);


--Relation Accidents
INSERT INTO relaccidents VALUES ('ZZ-00-97', 1);
INSERT INTO relaccidents VALUES ('ZZ-00-WW', 1);
INSERT INTO relaccidents VALUES ('ZZ-00-33', 2);
INSERT INTO relaccidents VALUES ('ZZ-00-SP', 2);
INSERT INTO relaccidents VALUES ('ZZ-BB-97', 3);
INSERT INTO relaccidents VALUES ('ZZ-BB-82', 4);
INSERT INTO relaccidents VALUES ('ZZ-AB-73', 5);
INSERT INTO relaccidents VALUES ('ZZ-BW-04', 6);
INSERT INTO relaccidents VALUES ('ZZ-BX-56', 7);

--Interchange messages
INSERT INTO interchange_message VALUES ('ZZ-00-97', 1, 'Road security' ,'Red traffic light', '2020-04-9', '{"lat" : "35.2", "long" : "47.2"}', 1);
INSERT INTO interchange_message VALUES ('ZZ-BB-97', 2, 'Road security' ,'Pedestrian hurts', '2020-05-09', '{"lat" : "38.1", "long" : "48.2"}', 2);
INSERT INTO interchange_message VALUES ('ZZ-BB-82', 2, 'Road security' ,'Baby crying', '2020-05-11', '{"lat" : "38.1", "long" : "49.2"}', 1);
INSERT INTO interchange_message VALUES ('ZZ-00-WW', 1, 'Road security' ,'Pedestrian hurts', '2020-05-13', '{"lat" : "35.2", "long" : "41.2"}', 1);
INSERT INTO interchange_message VALUES ('ZZ-BB-82', 1, 'Road security' ,'Baby crying', '2020-05-15', '{"lat" : "37.2", "long" : "42.2"}', 2);
INSERT INTO interchange_message VALUES ('ZZ-BB-97', 1, 'Road security' ,'Red traffic light', '2020-05-17', '{"lat" : "39.2", "long" : "44.2"}', 1);
INSERT INTO interchange_message VALUES ('ZZ-00-33', 1, 'Road security' ,'Red traffic light', '2020-05-19', '{"lat" : "38.2", "long" : "47.3"}', 1);

